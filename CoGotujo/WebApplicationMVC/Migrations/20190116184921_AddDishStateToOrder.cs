﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplicationMVC.Migrations
{
    public partial class AddDishStateToOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DishStateID",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DishStateID",
                table: "Orders",
                column: "DishStateID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders",
                column: "DishStateID",
                principalTable: "DishStates",
                principalColumn: "DishStateID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_DishStateID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DishStateID",
                table: "Orders");
        }
    }
}
