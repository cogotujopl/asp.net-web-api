﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplicationMVC.Migrations
{
    public partial class AddRequiredAnnotations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes");

            migrationBuilder.DropColumn(
                name: "SimpleIngredientName",
                table: "SimpleIngredients");

            migrationBuilder.AlterColumn<int>(
                name: "OrderID",
                table: "SingleDishes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "SingleDishes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SimpleIngredients",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "DishStateID",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Job",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "State",
                table: "DishStates",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Unit",
                table: "DishIngredients",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SimpleIngredientID",
                table: "DishIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "DishIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Dishes",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Dishes",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients",
                column: "SimpleIngredientID",
                principalTable: "SimpleIngredients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders",
                column: "DishStateID",
                principalTable: "DishStates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SimpleIngredients");

            migrationBuilder.AlterColumn<int>(
                name: "OrderID",
                table: "SingleDishes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "SingleDishes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "SimpleIngredientName",
                table: "SimpleIngredients",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DishStateID",
                table: "Orders",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "Job",
                table: "Employees",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Employees",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "State",
                table: "DishStates",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Unit",
                table: "DishIngredients",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "SimpleIngredientID",
                table: "DishIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "DishIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Dishes",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Dishes",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients",
                column: "SimpleIngredientID",
                principalTable: "SimpleIngredients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DishStates_DishStateID",
                table: "Orders",
                column: "DishStateID",
                principalTable: "DishStates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
