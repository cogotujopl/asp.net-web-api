﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplicationMVC.Migrations
{
    public partial class RenamePrimaryKeysToIDAndChangeForeignKeysToRelatedObjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SingleDishID",
                table: "SingleDishes",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "SimpleIngredientID",
                table: "SimpleIngredients",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "OrderID",
                table: "Orders",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "EmployeeID",
                table: "Employees",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "DishStateID",
                table: "DishStates",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "DishIngredientID",
                table: "DishIngredients",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "DishID",
                table: "Dishes",
                newName: "ID");

            migrationBuilder.AlterColumn<int>(
                name: "OrderID",
                table: "SingleDishes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "SingleDishes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SimpleIngredientID",
                table: "DishIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "DishIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_SingleDishes_DishID",
                table: "SingleDishes",
                column: "DishID");

            migrationBuilder.CreateIndex(
                name: "IX_SingleDishes_OrderID",
                table: "SingleDishes",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_DishIngredients_DishID",
                table: "DishIngredients",
                column: "DishID");

            migrationBuilder.CreateIndex(
                name: "IX_DishIngredients_SimpleIngredientID",
                table: "DishIngredients",
                column: "SimpleIngredientID");

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients",
                column: "SimpleIngredientID",
                principalTable: "SimpleIngredients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes",
                column: "DishID",
                principalTable: "Dishes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_Dishes_DishID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_DishIngredients_SimpleIngredients_SimpleIngredientID",
                table: "DishIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Dishes_DishID",
                table: "SingleDishes");

            migrationBuilder.DropForeignKey(
                name: "FK_SingleDishes_Orders_OrderID",
                table: "SingleDishes");

            migrationBuilder.DropIndex(
                name: "IX_SingleDishes_DishID",
                table: "SingleDishes");

            migrationBuilder.DropIndex(
                name: "IX_SingleDishes_OrderID",
                table: "SingleDishes");

            migrationBuilder.DropIndex(
                name: "IX_DishIngredients_DishID",
                table: "DishIngredients");

            migrationBuilder.DropIndex(
                name: "IX_DishIngredients_SimpleIngredientID",
                table: "DishIngredients");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "SingleDishes",
                newName: "SingleDishID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "SimpleIngredients",
                newName: "SimpleIngredientID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Orders",
                newName: "OrderID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Employees",
                newName: "EmployeeID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "DishStates",
                newName: "DishStateID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "DishIngredients",
                newName: "DishIngredientID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Dishes",
                newName: "DishID");

            migrationBuilder.AlterColumn<int>(
                name: "OrderID",
                table: "SingleDishes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "SingleDishes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SimpleIngredientID",
                table: "DishIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DishID",
                table: "DishIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
