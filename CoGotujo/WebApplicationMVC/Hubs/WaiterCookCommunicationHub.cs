﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Controllers;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.DTO;
using WebApplicationMVC.Models.Services;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Models.ViewModels;

namespace WebApplicationMVC.Hubs
{
    public class WaiterCookCommunicationHub : Hub
    {
        private readonly IDishService _dishService;
        private readonly IOrderService _orderService;
        private readonly IDishStateService _dishStateService;

        private static IList<DishState> _dishStates;

        public WaiterCookCommunicationHub(IDishService dishService,
            IOrderService orderService, IDishStateService dishStateService)
        {
            _dishService = dishService;
            _orderService = orderService;
            _dishStateService = dishStateService;

            _dishStates = _dishStateService.GetAllDishStates();
        }

        #region Waiter       
        public async Task AcceptOrder(string waiterViewModel)
        {
            var model = JsonConvert.DeserializeObject<WaiterViewModel>(waiterViewModel);
            await SaveNewOrderToDatabase(model);

            await SendOrdersToCook();

            await Clients.All.SendAsync("WaiterAcceptsOrder");
        }

        public async Task SendOrdersToCook()
        {
            var ordersForCook = _orderService.GetForCook();
            var ordersJson = JsonConvert.SerializeObject(ordersForCook.Select(order => OrderDTO.GetFromOrder(order)));
            await Clients.All.SendAsync("CookReceivesOrder", ordersJson);
        }

        private async Task SaveNewOrderToDatabase(WaiterViewModel model)
        {
            var dishes = _dishService.GetByIds(model.DishIndices);

            var order = new Order()
            {
                Table = model.Table,
                SingleDishes = new List<SingleDish>(),
                DishState = _dishStates.First(state => state.State == DishStates.Preparing.ToString())
            };

            foreach (var dish in dishes)
            {
                var dishCount = model.DishIndices.Count(index => index == dish.ID);
                for (int i = 0; i < dishCount; i++)
                {
                    order.SingleDishes.Add(new SingleDish()
                    {
                        Order = order,
                        Dish = dish
                    });
                }
            }
            await _orderService.AddOrderAsync(order);
        }

        #endregion

        #region Cook
        public async Task SendReady(int id)
        {
            var order = await _orderService.GetByIdAsync(id);
            SaveReadyOrderToDatabase(order);

            var ordersForWaiter = _orderService.GetForWaiter();
            var ordersJson = JsonConvert.SerializeObject(ordersForWaiter.Select(ord => OrderDTO.GetFromOrder(ord)));
            await Clients.All.SendAsync("WaiterReceivesReadyOrder", ordersJson);

            await Clients.All.SendAsync("CookSendsReadyOrder");
        }

        private void SaveReadyOrderToDatabase(Order order)
        {
            var dishStates = _dishStateService.GetAllDishStates();
            order.DishState = dishStates.Single(s => s.State == DishStates.Ready.ToString());
            _orderService.Update(order);
        }
        #endregion
    }
}
