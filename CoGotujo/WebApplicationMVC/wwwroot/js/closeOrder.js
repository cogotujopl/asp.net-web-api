﻿"use strict";

document.getElementById("closeOrder").addEventListener("click", function () {
    var id = document.getElementById("orderID").textContent;
    postAndRedirect("/Waiter/CloseAndSave", { id: id });
});