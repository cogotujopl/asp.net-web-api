﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/waiterCookCommunicationHub").build();

connection.start().catch(function (err) {
    return console.error(err.toString());
});


var dishIndices = [];

document.getElementById("addSingleDish").addEventListener("click", function (event) {
    var list = document.getElementById("selectedDish");
    var dish = list.options[list.selectedIndex];
    var li = document.createElement("li");
    li.textContent = dish.text;
    document.getElementById("orderedDishes").appendChild(li);
    dishIndices.push(dish.value);
});

document.getElementById("acceptOrderButton").addEventListener("click", function (event) {
    event.preventDefault();
    var table = document.getElementById("table").value;
    var waiterViewModel = {
        "Table": table,
        "DishIndices": dishIndices
    };
    connection.invoke("AcceptOrder", JSON.stringify(waiterViewModel)).then(function (resolved) {
        console.log("Finished");
        dishIndices = [];
        window.location.href = "/Waiter/Index";
    }).catch(function (err) {
        return console.error(err.toString());
    });
});