﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/waiterCookCommunicationHub").build();

connection.start().catch(function (err) {
    return console.error(err.toString());
});

///////////////
///Kucharz
///////////////
Array.prototype.groupBy = function (prop) {
    return this.reduce(function (groups, item) {
        const val = item[prop];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
    }, []);
};

connection.on("CookReceivesOrder", function (ordersJson) {
    var orders = JSON.parse(ordersJson);
    var parentDiv = document.getElementById("orders");
    parentDiv.className = "col-xs-3";
    removeChildren(parentDiv);
    orders.forEach((order, i, arr) => {
        var div = document.createElement("div");
        div.id = "order-" + order.ID;
        div.className = "table-responsive";

        var table = document.createElement("table");
        table.className = "table table-striped tableFixed";
        var thead = document.createElement("thead");
        thead.className = "orderTitle";
        var tr = document.createElement("tr");
        var th = document.createElement("th");

        th.textContent = "Stolik " + order.Table;
        tr.appendChild(th);
        th = document.createElement("th");
        th.textContent = "Liczba";
        tr.appendChild(th);
        thead.appendChild(tr);

        var td;
        var tbody = document.createElement("tbody");
        var dishes = [];
        order.Dishes.forEach((singleDish, ind, array) => {
            dishes.push(singleDish.Dish);
        });
        dishes.groupBy("ID").forEach((dish, ind, array) => {
            tr = document.createElement("tr");
            td = document.createElement("td");
            td.textContent = dish[0].Name;
            tr.appendChild(td);
            td = document.createElement("td");
            td.textContent = dish.length;
            tr.appendChild(td);

            tbody.appendChild(tr);
        });

        tr = document.createElement("tr");
        td = document.createElement("td");

        var button = document.createElement("button");
        button.className = "btn btn-success sendReadyButton";
        button.type = "button";        
        button.onclick = function () {
            sendReady(order.ID);
        };
        button.textContent = "Zaznacz jako gotowe";
        td.appendChild(button);

        tr.append(td);
        td = document.createElement("td");
        tr.append(td);

        tbody.append(tr);
        
        table.appendChild(thead);
        table.appendChild(tbody);

        div.appendChild(table);
        parentDiv.appendChild(div);
    });
    addListenersToSendReadyButtons();
});

connection.on("CookSendsReadyOrder", function () {
    window.location.href = "/Cook/Index";
}); 

function sendReady(order) {
    var orderJson = JSON.stringify(order);
    connection.invoke("SendReady", orderJson).then(function (resolved) {
        console.log("Order sent: ", orderJson);
    }).catch(function (err) {
        return console.error(err.toString());
    });
}

function removeChildren(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

//window.onload = addListenersToSendReadyButtons();

//function addListenersToSendReadyButtons() {
//    var sendReadyButtons = document.getElementsByClassName("sendReadyButton");
//    for (var i = 0; i < sendReadyButtons.length; i++) {
//        sendReadyButtons[i].addEventListener("click", function (event) {
//            console.log("W senReadyButton");
//        });
//    }    
//}

///////////////
