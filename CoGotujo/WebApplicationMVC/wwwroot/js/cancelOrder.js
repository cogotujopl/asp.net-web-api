﻿"use strict";

document.getElementById("cancelOrder").addEventListener("click", function () {
    var id = document.getElementById("orderID").textContent;
    postAndRedirect("/Waiter/CancelAndSave", { id: id });
});