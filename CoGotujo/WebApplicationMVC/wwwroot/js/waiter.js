﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/waiterCookCommunicationHub").build();

connection.start().catch(function (err) {
    return console.error(err.toString());
});



connection.on("ReceiveOrderStatus", function (orderNumber, status) {
    var msg = status.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = orderNumber + " says " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("readyOrdersList").appendChild(li);
});

connection.on("WaiterAcceptsOrder", function () {
    window.location.href = "/Waiter/Index";
});


connection.on("WaiterReceivesReadyOrder", function (ordersJson) {
    var orders = JSON.parse(ordersJson);

    var table = document.createElement("table");
    table.className = "table table-striped tableFixed";

    var headers = ["Stolik", "Zamknij zamówienie", "Anuluj zamówienie", "Aktualny status"];
    var thead = createThead(headers);
    table.appendChild(thead);

    var tbody = createTbody(orders);
    table.appendChild(tbody);

    var divForOrders = document.getElementById("orders");
    divForOrders.className = "table-responsive";
    removeChildren(divForOrders);
    divForOrders.appendChild(table);

    setCancelButtons();
    setCloseButtons();
});

function createTbody(orders) {
    var tbody = document.createElement("tbody");
    var tr;
    var td;

    orders.forEach((order, index, arr) => {
        tr = document.createElement("tr");
        var cells = [order.Table,
            "<button type=\"button\" class=\"btn btn-success closeButton\" id=\"close-order-" + order.ID + "\">Zamknij</button>",
            "<button type=\"button\" class=\"btn btn-danger cancelOrderButton\" id=\"cancel-order-" + order.ID + "\">Anuluj</button>",
            order.DishState.State
        ];
        cells.forEach((cell, i, arr) => {
            td = document.createElement("td");
            td.innerHTML = cell;
            tr.appendChild(td);
        });

        tbody.appendChild(tr);
    });    
    return tbody;
}

function createThead (headers) {
    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    var th = document.createElement("th");

    headers.forEach((header, i, arr) => {
        th = document.createElement("th");
        th.textContent = header;
        tr.append(th);
    });
    thead.appendChild(tr);
    thead.className = "orderTitle";
    return thead;
}

function removeChildren(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

//nieużywane, ale zostawiam na przyszłość do celów edukacyjnych, działa :D
//pod url można podłożyć ścieżkę do metody kontrolera np. /Waiter/Index
//data to obiekt javascriptowy
function sendPost(data, url) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    var toSend = JSON.stringify(data);
    xhr.send(toSend);
}

window.onload = function () {
    setCloseButtons();
    setCancelButtons();
};

function setCloseButtons() {
    var closeButtons = document.getElementsByClassName("closeButton");
    for (var i = 0; i < closeButtons.length; i++) {
        closeButtons[i].addEventListener("click", function (event) {
            var id = this.id.substring(12);            
            postAndRedirect("/Waiter/Close", { id: id });
        });
    }
}

function setCancelButtons() {
    var cancelButtons = document.getElementsByClassName("cancelOrderButton");
    for (var i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener("click", function (event) {
            var id = this.id.substring(13);
            postAndRedirect("/Waiter/Cancel", { id: id });
        });
    }
}

