﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplicationMVC.Models;
using WebApplicationMVC.Models.Services.Interfaces;

namespace WebApplicationMVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if(User.IsInRole("Admin"))
            {
                RedirectToActionResult redirectResult = new RedirectToActionResult("Index", "Admin", new { @area = "Admin"});
                return redirectResult;
            }

            if(User.IsInRole("Cook"))
            {
                RedirectToActionResult redirectResult = new RedirectToActionResult("Index", "Cook", new { @area = "Cook"});
                return redirectResult;
            }

            if(User.IsInRole("Waiter"))
            {
                RedirectToActionResult redirectResult = new RedirectToActionResult("Index", "Waiter", new { @area = "Waiter"});
                return redirectResult;
            }
            else 
                return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
