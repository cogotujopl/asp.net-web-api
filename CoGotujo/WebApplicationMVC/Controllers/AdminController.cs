using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database.IdentityModels;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Models.ViewModels;

namespace WebApplicationMVC.Controllers
{
    public class AdminController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IOrderService _orderService;
        private readonly UserManager<User> _userManager;


        public AdminController(IEmployeeService employeeService, IOrderService orderService, UserManager<User> userManager)
        {
            _employeeService = employeeService;
            _orderService = orderService;
            _userManager = userManager;
        }

        [Authorize(Roles="Admin")]
        public IActionResult Index()
        {
            var model = new AdminIndexViewModel();
            model.EmployeesList = _employeeService.GetAllEmployees();
            model.OrdersList = _orderService.GetAllOrders();
            return View(model);
        }

        [HttpPost]
        public IActionResult EditEmployee(AdminIndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Email, Email = model.Email, Role = model.Role };
                _userManager.AddToRoleAsync(user, user.Role);//parujemy użytkownika z nową rolą

                var employee = new Employee{ID = model.EmployeeID, Job = model.Role, Wage = model.Wage, Email = model.Email, PersonalInfo = model.PersonalData};
                _employeeService.Update(employee);
            }

            return RedirectToAction(nameof(AdminController.Index), "Admin");
        }


        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        
        public IActionResult Forbidden()
        {
            return View();
        }
    }
}