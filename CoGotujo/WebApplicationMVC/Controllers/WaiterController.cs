using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Hubs;
using WebApplicationMVC.Models.DTO;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Models.ViewModels;

namespace WebApplicationMVC.Controllers
{
    public class WaiterController : Controller
    {
        private readonly IDishService _dishService;
        private readonly IOrderService _orderService;
        private readonly IDishStateService _dishStateService;

        private static OrderDTO _currentOrder;

        public WaiterController(IDishService dishService,
            IOrderService orderService,
            IDishStateService dishStateService)
        {
            _dishService = dishService;
            _orderService = orderService;
            _dishStateService = dishStateService;
        }

        [Authorize(Roles = "Waiter")]
        public IActionResult Index()
        {
            var ordersToDisplay = _orderService.GetForWaiter()
                .Select(x => OrderDTO.GetFromOrder(x)).ToList();
            return View(ordersToDisplay);
        }
        
        [Authorize(Roles = "Waiter")]
        public IActionResult Add()
        {
            var waiterViewModel = GetEmptyViewModel();
            return View(waiterViewModel);
        }       

        [Authorize(Roles = "Waiter")]
        [HttpPost]
        public async Task<IActionResult> Close(int id)
        {
            var order = await _orderService.GetByIdAsync(id);
            _currentOrder = OrderDTO.GetFromOrder(order);
            return View(_currentOrder);
        }        

        [Authorize(Roles = "Waiter")]
        [HttpPost]
        public async Task<IActionResult> Cancel(int id)
        {
            var order = await _orderService.GetByIdAsync(id);
            _currentOrder = OrderDTO.GetFromOrder(order);
            return View(_currentOrder);
        }

        [Authorize(Roles = "Waiter")]
        [HttpPost]
        public async Task<IActionResult> CloseAndSave(int id)
        {
            await UpdateOrderStatus(id, DishStates.Close);
            return RedirectToAction("Index", "Waiter");
        }

        [Authorize(Roles = "Waiter")]
        [HttpPost]
        public async Task<IActionResult> CancelAndSave(int id)
        {
            await UpdateOrderStatus(id, DishStates.Cancelled);
            return RedirectToAction("Index", "Waiter");
        }

        private async Task UpdateOrderStatus(int orderId, DishStates state)
        {
            if (_currentOrder == null || _currentOrder.ID != orderId)
            {
                var ord = await _orderService.GetByIdAsync(orderId);
                _currentOrder = OrderDTO.GetFromOrder(ord);
            }
            var order = OrderDTO.GetFromOrderDTO(_currentOrder);
            var dishStates = _dishStateService.GetAllDishStates();
            order.DishState = dishStates.Single(s => s.State == state.ToString());
            _orderService.Update(order);
            _currentOrder = null;
        }

        private WaiterViewModel GetEmptyViewModel()
        {
            return new WaiterViewModel()
            {
                Menu = _dishService.GetAllDishes().Select(dish => new SelectListItem()
                {
                    Value = dish.ID.ToString(),
                    Text = dish.Name,
                    Selected = true
                }).ToList(),
            };
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}