using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.DTO;
using WebApplicationMVC.Models.Services.Interfaces;

namespace WebApplicationMVC.Controllers
{
    public class CookController : Controller
    {
        private readonly IOrderService _orderService;

        public CookController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [Authorize(Roles = "Cook")]
        public IActionResult Index()
        {
            var ordersToDisplay = _orderService.GetForCook()
                .Select(x => OrderDTO.GetFromOrder(x)).ToList();
            return View(ordersToDisplay);
        }

        public IActionResult Error()
        {
            return View();
        }
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}