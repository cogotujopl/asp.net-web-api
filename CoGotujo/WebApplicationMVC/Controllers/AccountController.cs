using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models;
using WebApplicationMVC.Models.Database.IdentityModels;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Models.ViewModels;

namespace WebApplicationMVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly IEmployeeService _employeeService;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILoggerFactory loggerFactory,
            IEmployeeService employeeService,
            RoleManager<IdentityRole> roleManager)
            {
                _userManager = userManager;
                _signInManager = signInManager;
                _logger = loggerFactory.CreateLogger<AccountController>();
                _employeeService = employeeService;
                _roleManager = roleManager;
            }


        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email,
                    model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation(1, "User logged in.");

                    User actualUser = await _userManager.FindByEmailAsync(model.Email);
                    var userRole = actualUser.Role;
                    return RedirectToUserPage(returnUrl, userRole);

                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return View("Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private IActionResult RedirectToUserPage(string returnUrl, string userRole)
        {
            switch (userRole)
            {
                case nameof(AvailableRoles.Admin):
                    {
                        return RedirectToAction(nameof(AdminController.Index), "Admin");
                    }
                case nameof(AvailableRoles.Cook):
                    {
                        return RedirectToAction(nameof(CookController.Index), "Cook");
                    }
                case nameof(AvailableRoles.Waiter):
                    {
                        return RedirectToAction(nameof(WaiterController.Index), "Waiter");
                    }

                default:
                    return RedirectToLocal(returnUrl);
            }
        }


        // GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Email, Email = model.Email, Role = model.Role };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {                  
                    if(! await _roleManager.RoleExistsAsync(user.Role))//sprawdza istnienie roli
                    {
                        var userRole = new IdentityRole(user.Role);
                        var resultRole = await _roleManager.CreateAsync(userRole); //utworzenie roli jeśli nie istniała
                        if(resultRole.Succeeded)
                        {
                           await _userManager.AddToRoleAsync(user, userRole.ToString());//sparowanie użytkownika z rolą
                        }
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, user.Role); // rola istniała, parujemy z nią użytkownika
                    }  
                    string personalData = $"{model.Name} {model.Surname}";
                    await _employeeService.AddEmployeeAsync(user, personalData);
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, $"User created a new account with password and role {user.Role}.");
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // POST: /Account/LogOut
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();

            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(AccountController.Login), "Account");
        }

        // GET: /Account/Manage
        [HttpGet]
        [Route("Account/Manage")]
        [Authorize]
        public IActionResult Manage()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");//Ustawienie strony pojawiającej się po zalogowaniu
            }
        }
        #endregion
    }
}

