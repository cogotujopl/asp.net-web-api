namespace WebApplicationMVC.Models
{
    public enum AvailableRoles
    {
        Admin, 
        Waiter, 
        Cook 
    }
}