﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.DTO
{
    public class DishDTO
    {
        public int ID { get; set; }
        public Dish Dish { get; set; }

    }
}
