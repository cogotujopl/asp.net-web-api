﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.DTO
{
    public class OrderDTO
    {
        public int ID { get; set; }
        public DishState DishState { get; set; }
        public string Address { get; set; }
        public int Table { get; set; }
        public string AdditionalInfo { get; set; }

        public IEnumerable<DishDTO> Dishes { get; set; }

        public static OrderDTO GetFromOrder(Order order)
        {
            return new OrderDTO()
            {
                ID = order.ID,
                DishState = order.DishState,
                Address = order.Address,
                Table = order.Table,
                AdditionalInfo = order.AdditionalInfo,
                Dishes = order.SingleDishes.Select(x => new DishDTO()
                {
                    ID = x.ID,
                    Dish = x.Dish
                })
            };
        }

        public static Order GetFromOrderDTO(OrderDTO order)
        {
            return new Order()
            {
                ID = order.ID,
                DishState = order.DishState,
                Address = order.Address,
                Table = order.Table,
                AdditionalInfo = order.AdditionalInfo,
                SingleDishes = order.Dishes.Select(x => new SingleDish()
                {
                    ID = x.ID,
                    Dish = x.Dish
                }).ToList()
            };
        }
    }
}
