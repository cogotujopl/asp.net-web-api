using System;
using System.Collections.Generic;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;


namespace WebApplicationMVC.Models.Services
{
    public class DishStateService : IDishStateService
    {
        private readonly IDishStateRepository _dishStateRepository;

        public DishStateService(IDishStateRepository dishStateRepository)
        {
            _dishStateRepository = dishStateRepository;
        }

        public IList<DishState> GetAllDishStates()
        {
            return _dishStateRepository.GetAll();
        }

        public bool IsPossible(string stateName)
        {
            foreach (var state in Enum.GetValues(typeof(DishStates)))
            {
                if (stateName == state.ToString())
                    return true;
            }

            return false;
        }

    }   
}