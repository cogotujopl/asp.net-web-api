using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;


namespace WebApplicationMVC.Models.Services
{
    public class DishService : IDishService
    {
        private readonly IDishRepository _dishRepository;

        public DishService(IDishRepository dishRepository)
        {
            _dishRepository = dishRepository;
        }

        public bool IsPricePositive(decimal price)
        {
            return (price > 0) ? true : false;
        }

        public bool IsCookingTimePositive(int time)
        {
            return (time > 0) ? true : false;
        }

        public IList<Dish> GetAllDishes()
        {
            return _dishRepository.GetAll();
        }

        public IList<Dish> GetByIds(IEnumerable<int> dishIndexes)
        {
             return _dishRepository.GetByIds(dishIndexes);
        }

        public async Task AddAsync(Dish item)
        {
            if (IsPricePositive(item.DishPrice) && IsCookingTimePositive(item.CookingTime))
                await _dishRepository.AddAsync(item);
        }

        public void Remove(Dish item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _dishRepository.ExistsAsync(item.ID));
            if (task.Result)
                _dishRepository.Remove(item);
        }

        public void Update(Dish item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _dishRepository.ExistsAsync(item.ID));
            if (task.Result)
                _dishRepository.Update(item);
        }

        public IList<Dish> GetByIngredient(SimpleIngredient ingredient)
        {
            return _dishRepository.GetByIngredient(ingredient);
        }


    }
}