using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database.IdentityModels;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;


namespace WebApplicationMVC.Models.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task AddEmployeeAsync(User user, string personalData)
        {
            var employee = new Employee()
            {
                Email = user.Email,
                Job = user.Role,
                PersonalInfo = personalData                
            };
            await _employeeRepository.AddAsync(employee);
        }

        public IList<Employee> GetAllEmployees()
            =>  _employeeRepository.GetAll();

        public async Task<bool> IsNameAndSurnameAvailableAsync(string name)
            => await _employeeRepository.ExistsAsync(name.ToLowerInvariant()) == false;

        public bool WageIsPositive(decimal wage)
        {
            return (wage > 0) ? true : false;
        }

        public void Update(Employee item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _employeeRepository.ExistsAsync(item.ID));
            if (task.Result)
                _employeeRepository.Update(item);
        }

        public void Remove(Employee item)
        {
                Task<bool> task = Task.Run<bool>(async () => await _employeeRepository.ExistsAsync(item.ID));
                if (task.Result)
                    _employeeRepository.Remove(item);
        }

        public async Task AddEmployeeAsync(User user)
        {
            var employee = new Employee()
            {
                Email = user.Email,
                Job = user.Role,
            };
            await _employeeRepository.AddAsync(employee);
        }
    }
}