using System.Threading.Tasks;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Database.Models;


namespace WebApplicationMVC.Models.Services
{
    public class SimpleIngredientService : ISimpleIngredientService
    {
        private readonly ISimpleIngredientRepository _simpleIngredientRepository;

        public SimpleIngredientService(ISimpleIngredientRepository simpleIngredientRepository)
        {
            _simpleIngredientRepository = simpleIngredientRepository;
        }

        public async void Add(SimpleIngredient simpleIngredient)
        {               
            await _simpleIngredientRepository.AddAsync(simpleIngredient);
        }

        public void Update(SimpleIngredient item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _simpleIngredientRepository.ExistsAsync(item.ID));
            if (task.Result)
                _simpleIngredientRepository.Update(item);
        }

        public void Remove(SimpleIngredient item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _simpleIngredientRepository.ExistsAsync(item.ID));
            if (task.Result)
                _simpleIngredientRepository.Remove(item);
        }

    }
}