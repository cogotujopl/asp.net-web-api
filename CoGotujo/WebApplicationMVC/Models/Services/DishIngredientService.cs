using System.Threading.Tasks;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Database.Models;
using System.Collections.Generic;

namespace WebApplicationMVC.Models.Services
{
    public class DishIngredientService : IDishIngredientService
    {
        private readonly IDishIngredientRepository _dishIngredientRepository;

        public DishIngredientService(IDishIngredientRepository dishIngredientRepository)
        {
            _dishIngredientRepository = dishIngredientRepository;
        }

        public bool amountIsPositive(int amount)
        {
            return (amount > 0) ? true : false;
        }

        public async Task<DishIngredient> GetByIdAsync(int id)
        {
            return await _dishIngredientRepository.GetByIdAsync(id);
        }

        public IList<DishIngredient> GetByName(string name)
        {
            return _dishIngredientRepository.GetByName(name);
        }

        public IList<DishIngredient> GetAll()
        {
            return _dishIngredientRepository.GetAll();
        }

        public async Task AddAsync(DishIngredient item)
        {
            await _dishIngredientRepository.AddAsync(item);
        }

        public void Remove(DishIngredient item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _dishIngredientRepository.ExistsAsync(item.ID));
            if (task.Result)
                _dishIngredientRepository.Remove(item);
        }

        public void Update(DishIngredient item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _dishIngredientRepository.ExistsAsync(item.ID));
            if (task.Result)
                _dishIngredientRepository.Update(item);
        }

    }
}