using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface ISingleDishService
    {
        Task AddSingleDishAsync(SingleDish singleDish);
        void Update(SingleDish item);
        void Remove(SingleDish item);
    }
}