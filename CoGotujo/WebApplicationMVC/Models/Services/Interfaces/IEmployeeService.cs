using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database.IdentityModels;

namespace WebApplicationMVC.Models.Services.Interfaces
{    
    public interface IEmployeeService
    {
        bool WageIsPositive(decimal wage);
        Task AddEmployeeAsync(User user, string personalData);
        IList<Employee> GetAllEmployees();
        Task AddEmployeeAsync(User user);
        void Update(Employee item);
        void Remove(Employee item);

    }
}