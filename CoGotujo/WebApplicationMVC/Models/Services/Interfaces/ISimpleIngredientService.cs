using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface ISimpleIngredientService
    {
        void Add(SimpleIngredient simpleIngredient);
        void Update(SimpleIngredient item);
        void Remove(SimpleIngredient item);

    }
}