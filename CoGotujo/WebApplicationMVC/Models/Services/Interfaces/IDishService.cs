using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface IDishService
    {
        bool IsPricePositive(decimal price);
        bool IsCookingTimePositive(int time);
        IList<Dish> GetAllDishes();
        IList<Dish> GetByIds(IEnumerable<int> dishIndexes);
        Task AddAsync(Dish item);
        void Remove(Dish item);
        void Update(Dish item);
        IList<Dish> GetByIngredient(SimpleIngredient ingredient);
    }
}