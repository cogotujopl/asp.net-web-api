using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface IDishIngredientService
    {
        bool amountIsPositive(int amount);
        Task<DishIngredient> GetByIdAsync(int id);
        IList<DishIngredient> GetByName(string name);
        IList<DishIngredient> GetAll();
        Task AddAsync(DishIngredient item);
        void Remove(DishIngredient item);
        void Update(DishIngredient item);

    }

}