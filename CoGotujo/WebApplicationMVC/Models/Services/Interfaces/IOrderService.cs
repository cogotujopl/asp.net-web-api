using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface IOrderService
    {
        bool TableNumberIsPositive(int tableNumber);
        Task AddOrderAsync(Order order);
        IList<Order> GetByDishStates(List<DishStates> dishStates);
        IList<Order> GetForWaiter();
        IList<Order> GetForCook();
        IList<Order> GetAllOrders();
        void Update(Order item);
        void Remove(Order item);
        Task<Order> GetByIdAsync(int id);
    }
}