using System.Collections.Generic;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Services.Interfaces
{
    public interface IDishStateService
    {
        bool IsPossible(string stateName);
        IList<DishState> GetAllDishStates();

    }
}