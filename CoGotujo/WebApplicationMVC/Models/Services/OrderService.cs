using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;

namespace WebApplicationMVC.Models.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task AddOrderAsync(Order order)
        {
            await _orderRepository.AddAsync(order);
        }

        public IList<Order> GetAllOrders()
        {
            return _orderRepository.GetAll();
        }

        public IList<Order> GetByDishStates(List<DishStates> dishStates)
        {
            return _orderRepository.GetByDishStates(dishStates);
        }

        public IList<Order> GetForWaiter()
        {
            return GetByDishStates(new List<DishStates>()
                {
                    DishStates.New,
                    DishStates.Preparing,
                    DishStates.Ready
                });
        }

        public IList<Order> GetForCook()
        {
            return GetByDishStates(new List<DishStates>()
                {
                    DishStates.New,
                    DishStates.Preparing,
                });
        }

        public bool TableNumberIsPositive(int tableNumber)
        {
            return (tableNumber > 0) ? true : false;
        }

        public void Update(Order item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _orderRepository.ExistsAsync(item.ID));
            if (task.Result)
                _orderRepository.Update(item);
        }

        public void Remove(Order item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _orderRepository.ExistsAsync(item.ID));
            if (task.Result)
                _orderRepository.Remove(item);
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _orderRepository.GetByIdAsync(id);
        }
    }
}