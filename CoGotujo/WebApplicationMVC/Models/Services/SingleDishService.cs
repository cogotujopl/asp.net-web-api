using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Services.Interfaces;


namespace WebApplicationMVC.Models.Services
{
    public class SingleDishService : ISingleDishService
    {
        private readonly ISingleDishRepository _singleDishRepository;

        public SingleDishService(ISingleDishRepository singleDishRepository)
        {
            _singleDishRepository = singleDishRepository;
        }

        public async Task AddSingleDishAsync(SingleDish singleDish)
        {
            await _singleDishRepository.AddAsync(singleDish);
        }

        public void Update(SingleDish item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _singleDishRepository.ExistsAsync(item.ID));
            if (task.Result)
                _singleDishRepository.Update(item);
        }

        public void Remove(SingleDish item)
        {
            Task<bool> task = Task.Run<bool>(async () => await _singleDishRepository.ExistsAsync(item.ID));
            if (task.Result)
                _singleDishRepository.Remove(item);
        }

    }
}