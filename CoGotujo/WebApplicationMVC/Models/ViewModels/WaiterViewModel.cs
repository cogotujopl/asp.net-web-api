﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.DTO;

namespace WebApplicationMVC.Models.ViewModels
{
    public class WaiterViewModel
    {
        public int Table { get; set; }
        public Dish Dish { get; set; }
        public List<int> DishIndices { get; set; }
        public string AdditionalInfo { get; set; }
        public List<OrderDTO> OrdersToDisplay { get; set; }

        public List<SelectListItem> Menu { get; set; }
    }
}
