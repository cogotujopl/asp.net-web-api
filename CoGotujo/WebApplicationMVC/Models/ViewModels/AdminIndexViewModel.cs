using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.ViewModels
{
    public class AdminIndexViewModel
    {
        public IList<Employee> EmployeesList {get; set;}
        public IList<Order> OrdersList {get; set;}

        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }

        [Required]
        [Display(Name = "EmployeeID")]
        public int EmployeeID { get; set; }

        [Required]
        [Range(0, (double)decimal.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DataType(DataType.Currency)]
        [Display(Name = "Wage")]
        public decimal Wage { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string PersonalData { get; set; }
    }
}