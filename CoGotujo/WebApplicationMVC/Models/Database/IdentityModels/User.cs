using Microsoft.AspNetCore.Identity;

namespace WebApplicationMVC.Models.Database.IdentityModels
{
    public class User : IdentityUser
    {
        [PersonalData]
        public string Name { get; set; }
        
        [PersonalData]
        public string Role { get; set; }
    }
}