using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Database
{
    public interface IDataContext
    {
        DbSet<Dish> Dishes { get; set; }
        DbSet<DishIngredient> DishIngredients { get; set; }
        DbSet<DishState> DishStates { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<SimpleIngredient> SimpleIngredients { get; set; }
        DbSet<SingleDish> SingleDishes { get; set; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}