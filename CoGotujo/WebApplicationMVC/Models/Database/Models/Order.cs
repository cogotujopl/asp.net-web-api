﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class Order
    {
        public int ID { get; set; }
        [Required]
        public DishState DishState { get; set; }
        public string Address { get; set; }
        [Required]
        public int Table { get; set; }
        public string AdditionalInfo { get; set; }

        public ICollection<SingleDish> SingleDishes { get; set; }

    }
}
