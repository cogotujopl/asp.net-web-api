﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class DishIngredient
    {
        public int ID { get; set; }
        [Required]
        public Dish Dish { get; set; }
        [Required]
        public SimpleIngredient SimpleIngredient { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public string Unit { get; set; }
    }
}
