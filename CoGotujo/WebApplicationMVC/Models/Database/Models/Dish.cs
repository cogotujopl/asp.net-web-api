﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplicationMVC.Database.Models
{
    public class Dish
    {
        public int ID { get; set; }
        [Required]
        public string Name {get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal DishPrice { get; set; }
        public int CookingTime { get; set; }
        public string PathToImage { get; set; }

        ICollection<DishIngredient> DishIngredients { get; set; }
    }
}
