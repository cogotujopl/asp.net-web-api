﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class SimpleIngredient
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
