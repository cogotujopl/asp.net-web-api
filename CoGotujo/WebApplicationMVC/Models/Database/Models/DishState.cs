﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class DishState
    {
        public int ID { get; set; }
        [Required]
        public string State { get; set; }
    }

    public enum DishStates
    {
        New,
        Preparing,
        Ready,
        Close,
        Cancelled
    }
}
