﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class SingleDish
    {
        public int ID { get; set; }
        [Required]
        public Dish Dish { get; set; }
        [Required]
        public Order Order { get; set; }
    }
}
