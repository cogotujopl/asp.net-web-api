﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVC.Database.Models
{
    public class Employee
    {
        public int ID { get; set; }
        [Required]
        public string Job { get; set; }
        public string PersonalInfo { get; set; }
        public decimal Wage { get; set; }
        [Required]
        public string Email { get; set; }
    }
}
