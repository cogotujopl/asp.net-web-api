﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Database.Context
{
    public class EFDatabaseContext : DbContext, IDataContext
    {
        public EFDatabaseContext(DbContextOptions<EFDatabaseContext> options)
            : base(options)
        { }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<DishIngredient> DishIngredients { get; set; }
        public DbSet<DishState> DishStates { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<SimpleIngredient> SimpleIngredients { get; set; }
        public DbSet<SingleDish> SingleDishes { get; set; }

        public async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }

        void IDataContext.SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
