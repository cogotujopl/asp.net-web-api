using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;

namespace WebApplicationMVC.Models.Repositories
{
    public class DishIngredientRepository : IDishIngredientRepository
    {
        private readonly IDataContext _database;

        public DishIngredientRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task<DishIngredient> GetByIdAsync(int id)
        {
            return await _database.DishIngredients.FindAsync(id);
        }

        public IList<DishIngredient> GetAll()
        {
            return _database.DishIngredients.ToList();
        }

        public async Task AddAsync(DishIngredient item)
        {
            await _database.DishIngredients.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(DishIngredient item)
        {
            _database.DishIngredients.Remove(item);
            _database.SaveChanges();
        }

        public void Update(DishIngredient item)
        {
            _database.DishIngredients.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> ExistsAsync(DishIngredient item)
        {
            return await _database.DishIngredients.AnyAsync(x =>
                x.SimpleIngredient.ID == item.SimpleIngredient.ID && x.Dish.ID == item.Dish.ID);
        }

        public async Task<bool> DishIDExistsAsync(DishIngredient item)
        {
            return await _database.DishIngredients.AnyAsync(x => x.Dish.ID == item.Dish.ID);
        }

        public async Task<bool> SimpleIngredientIDExistsAsync(DishIngredient item)
        {
            return await _database.DishIngredients.AnyAsync(x => x.SimpleIngredient.ID == item.SimpleIngredient.ID);
        }

        public IList<DishIngredient> GetByIds(IEnumerable<int> indexes)
        {
            return _database.DishIngredients.Where(dishIngredient => indexes.Contains(dishIngredient.ID)).ToList();
        }

        public IList<DishIngredient> GetByName(string name)
        {
            return _database.DishIngredients
                .Include(dishIngredient => dishIngredient.SimpleIngredient)
                .Where(dishIngredient => dishIngredient.SimpleIngredient.Name.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.DishIngredients.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.DishIngredients.AnyAsync(x => x.ID == id);
        }
    }
}