using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;

namespace WebApplicationMVC.Models.Repositories
{
    public class SingleDishRepository : ISingleDishRepository
    {
        private readonly IDataContext _database;
        public SingleDishRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task<SingleDish> GetByIdAsync(int id)
        {
            return await _database.SingleDishes.FindAsync(id);
        }

        public IList<SingleDish> GetAll()
        {
            return _database.SingleDishes.ToList();
        }

        public IList<Dish> GetByIngredient(SimpleIngredient simpleIngredient)
        {
            //Jesli nie dziala to pewnie trzeba wstawic Include'a przed 'Where'm
            List<int> dishIDs= _database.DishIngredients.Where(dishIngredient => dishIngredient.ID == simpleIngredient.ID).Select(x => x.Dish.ID).ToList();
            return _database.Dishes
                .Where(dish => dishIDs.Contains(dish.ID)).ToList();  
        }

        public async Task AddAsync(SingleDish item)
        {
            await _database.SingleDishes.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(SingleDish item)
        {
            _database.SingleDishes.Remove(item);
            _database.SaveChanges();
        }

        public void Update(SingleDish item)
        {
            _database.SingleDishes.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> DishIDExistsAsync(SingleDish item)
        {
            return await _database.SingleDishes.AnyAsync(x => x.Dish.ID == item.Dish.ID);
        }

        public async Task<bool> OrderIDExistsAsync(SingleDish item)
        {
            return await _database.Orders.AnyAsync(x => x.ID == item.Order.ID);
        }

        public IList<SingleDish> GetByIds(IEnumerable<int> indexes)
        {
            return _database.SingleDishes.Where(singleDish => indexes.Contains(singleDish.ID)).ToList();
        }

        public IList<SingleDish> GetByName(string name)
        {
            return _database.SingleDishes
                .Include(singleDish => singleDish.Order)
                .Include(SingleDish => SingleDish.Dish)
                .Where(singleDish => singleDish.Dish.Name.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.SingleDishes.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.SingleDishes.AnyAsync(x => x.ID == id);
        }
    }
}