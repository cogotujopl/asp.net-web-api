using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Database;
using System;
using WebApplicationMVC.Database.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApplicationMVC.Models.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IDataContext _database;
        public OrderRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            var orders = _database.Orders.Where(order => order.ID == id)
                .Include(order => order.DishState)
                .Include(order => order.SingleDishes)
                .ThenInclude(singleDish => singleDish.Dish);
            return await orders.FirstAsync();
        }

        public IList<Order> GetAll()
        {
            return _database.Orders
                .Include(order => order.DishState)
                .Include(order => order.SingleDishes)
                .ThenInclude(singleDish => singleDish.Dish)
                .ToList();
        }

        public async Task AddAsync(Order item)
        {
            await _database.Orders.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(Order item)
        {
            _database.Orders.Remove(item);
            _database.SaveChanges();
        }

        public void Update(Order item)
        {
            _database.Orders.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> StateIDExistsAsync(Order item)
        {
            return await _database.DishStates.AnyAsync(x => x.State == item.DishState.State);
        }

        public IList<Order> GetByIds(IEnumerable<int> indexes)
        {
            return _database.Orders.Where(order => indexes.Contains(order.ID))
                .Include(order => order.DishState)
                .Include(order => order.SingleDishes)
                .ThenInclude(singleDish => singleDish.Dish)
                .ToList();
        }

        public IList<Order> GetByDishStates(List<DishStates> dishStates)
        {
            var states = dishStates.Select(state => state.ToString());
            return _database.Orders
                .Include(x => x.DishState)
                .Include(x => x.SingleDishes)
                .ThenInclude(a => a.Dish)
                .Where(order => states.Contains(order.DishState.State)).ToList();
        }

        public IList<Order> GetByName(string name)
        {
            return _database.Orders
                .Include(order => order.DishState)
                .Include(order => order.SingleDishes)
                .ThenInclude(singleDish => singleDish.Dish)
                .Where(order => order.Address.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.Orders.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.Orders.AnyAsync(x => x.ID == id);
        }
    }
}