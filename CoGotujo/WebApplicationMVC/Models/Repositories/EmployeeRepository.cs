using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;


namespace WebApplicationMVC.Models.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IDataContext _database;
        public EmployeeRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task AddAsync(Employee item)
        {
            await _database.Employees.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public IList<Employee> GetAll()
        {
            return _database.Employees.ToList();
        }

        public async Task<Employee> GetByIdAsync(int id)
        {
            return await _database.Employees.FindAsync(id);
        }
        public void Remove(Employee item)
        {
            _database.Employees.Remove(item);
            _database.SaveChanges();
        }
        public void Update(Employee item)
        {
            _database.Employees.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> ExistsAsync(string name)
        {
            return await _database.Employees.AnyAsync(x => x.PersonalInfo == name);
        }

        public IList<Employee> GetByIds(IEnumerable<int> indexes)
        {
            return _database.Employees.Where(employee => indexes.Contains(employee.ID)).ToList();
        }

        public IList<Employee> GetByName(string name)
        {
            return _database.Employees.Where(employee => employee.PersonalInfo.Contains(name)).ToList();
        }

        public IList<Employee> GetByJob(string job)
        {
            return _database.Employees.Where(employee => employee.Job.Contains(job)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.Employees.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.Employees.AnyAsync(x => x.ID == id);
        }
    }
}