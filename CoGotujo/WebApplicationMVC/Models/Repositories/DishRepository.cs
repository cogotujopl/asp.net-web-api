using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;


namespace WebApplicationMVC.Models.Repositories
{
    public class DishRepository : IDishRepository
    {
        private readonly IDataContext _database;
        public DishRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task<Dish> GetByIdAsync(int id)
        {
            return await _database.Dishes.FindAsync(id);
        }

        public IList<Dish> GetAll()
        {
            return _database.Dishes.ToList();
        }

        public async Task AddAsync(Dish item)
        {
            await _database.Dishes.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(Dish item)
        {
            _database.Dishes.Remove(item);
            _database.SaveChanges();
        }

        public void Update(Dish item)
        {
            _database.Dishes.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> ExistsAsync(string name)
        {
            return await _database.Dishes.AnyAsync(x => x.Name == name);
        }

        public IList<Dish> GetByIds(IEnumerable<int> indexes)
        {
            return _database.Dishes.Where(dish => indexes.Contains(dish.ID)).ToList();
        }

        public IList<Dish> GetByName(string name)
        {
            return _database.Dishes.Where(dish => dish.Name.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.Dishes.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.Dishes.AnyAsync(x => x.ID == id);
        }

        public IList<Dish> GetByIngredient(SimpleIngredient simpleIngredient)
        {
            //Jesli nie dziala to pewnie trzeba wstawic Include'a przed 'Where'm
            List<int> dishIDs = _database.DishIngredients.Where(dishIngredient => dishIngredient.ID == simpleIngredient.ID).Select(x => x.Dish.ID).ToList();
            return _database.Dishes
                .Where(dish => dishIDs.Contains(dish.ID)).ToList();
        }
    }
}