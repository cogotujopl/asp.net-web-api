using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface IDishIngredientRepository : IRepository<DishIngredient>
    {
        Task<bool> ExistsAsync(DishIngredient item);
        Task<bool> DishIDExistsAsync(DishIngredient item);
        Task<bool> SimpleIngredientIDExistsAsync(DishIngredient item);
    }
}