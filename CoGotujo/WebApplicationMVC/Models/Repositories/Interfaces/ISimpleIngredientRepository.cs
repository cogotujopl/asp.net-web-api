using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface ISimpleIngredientRepository : IRepository<SimpleIngredient>
    {
        Task<bool> ExistsAsync(string name);
    } 
}