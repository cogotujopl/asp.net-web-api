using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface ISingleDishRepository : IRepository<SingleDish>
    {
        Task<bool> DishIDExistsAsync(SingleDish item);
        Task<bool> OrderIDExistsAsync(SingleDish item);
    } 
}