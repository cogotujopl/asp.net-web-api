using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface IDishRepository : IRepository<Dish>
    {
        Task<bool> ExistsAsync(string name);
        IList<Dish> GetByIngredient(SimpleIngredient simpleIngredient);
    } 
}