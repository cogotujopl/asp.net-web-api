using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        Task<T> GetByIdAsync(int id);
        IList<T> GetByIds(IEnumerable<int> indexes);
        IList<T> GetByName(string name);
        IList<T> GetAll();
        Task AddAsync(T item);
        void Remove(T item);
        void Update(T item);
        Task<bool> ExistsAsync(int id);
    }
}