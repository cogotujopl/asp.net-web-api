using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<bool> StateIDExistsAsync(Order item);
        IList<Order> GetByDishStates(List<DishStates> dishStates);
    }
}