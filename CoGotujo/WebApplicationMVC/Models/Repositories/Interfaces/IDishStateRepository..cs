using WebApplicationMVC.Database.Models;

namespace WebApplicationMVC.Models.Repositories.Interfaces
{
    public interface IDishStateRepository : IRepository<DishState>
    {
        
    } 
}