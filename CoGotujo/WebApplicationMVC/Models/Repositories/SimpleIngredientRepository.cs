using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;

namespace WebApplicationMVC.Models.Repositories
{
    public class SimpleIngredientRepository : ISimpleIngredientRepository
    {
        private readonly IDataContext _database;
        public SimpleIngredientRepository(IDataContext database)
        {
            _database = database;
        }

        public async Task<SimpleIngredient> GetByIdAsync(int id)
        {
            return await _database.SimpleIngredients.FindAsync(id);
        }

        public IList<SimpleIngredient> GetAll()
        {
            return _database.SimpleIngredients.ToList();
        }

        public async Task AddAsync(SimpleIngredient item)
        {
            await _database.SimpleIngredients.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(SimpleIngredient item)
        {
            _database.SimpleIngredients.Remove(item);
            _database.SaveChanges();
        }

        public void Update(SimpleIngredient item)
        {
            _database.SimpleIngredients.Update(item);
            _database.SaveChanges();
        }

        public async Task<bool> ExistsAsync(string name)
        {
           return await _database.SimpleIngredients.AnyAsync(x => x.Name == name);
        }

        public IList<SimpleIngredient> GetByIds(IEnumerable<int> indexes)
        {
            return _database.SimpleIngredients.Where(simpleIngredient => indexes.Contains(simpleIngredient.ID)).ToList();
        }

        public IList<SimpleIngredient> GetByName(string name)
        {
            return _database.SimpleIngredients.Where(simpleIngredient => simpleIngredient.Name.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.SimpleIngredients.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await _database.SimpleIngredients.AnyAsync(x => x.ID == id);
        }
    }
}