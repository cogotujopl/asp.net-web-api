using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVC.Database.Models;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Models.Repositories.Interfaces;

namespace WebApplicationMVC.Models.Repositories
{
    public class DishStateRepository : IDishStateRepository
    {
        private readonly IDataContext _database;
        public DishStateRepository(IDataContext database)
        {
            _database = database;
        }


        public async Task<DishState> GetByIdAsync(int id)
        {
            return await _database.DishStates.FindAsync(id);
        }

        public IList<DishState> GetAll()
        {
            return _database.DishStates.ToList();
        }

        public async Task AddAsync(DishState item)
        {
            await _database.DishStates.AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public void Remove(DishState item)
        {
            _database.DishStates.Remove(item);
            _database.SaveChanges();
        }

        public void Update(DishState item)
        {
            _database.DishStates.Update(item);
            _database.SaveChanges();
        }

        public IList<DishState> GetByIds(IEnumerable<int> indexes)
        {
            return _database.DishStates.Where(dishState => indexes.Contains(dishState.ID)).ToList();
        }


        public IList<DishState> GetByName(string name)
        {
            return _database.DishStates.Where(dishState => dishState.State.Contains(name)).ToList();
        }

        public bool Exists(int id)
        {
            return _database.DishStates.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }

}