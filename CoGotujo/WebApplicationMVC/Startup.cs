﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using WebApplicationMVC.Models.Database.Context;
using WebApplicationMVC.Models.Repositories.Interfaces;
using WebApplicationMVC.Models.Repositories;
using WebApplicationMVC.Models.Services;
using WebApplicationMVC.Models.Services.Interfaces;
using WebApplicationMVC.Models.Database;
using WebApplicationMVC.Hubs;
using Microsoft.AspNetCore.Identity;
using WebApplicationMVC.Models.Database.IdentityModels;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace WebApplicationMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc(config=>
            {
                var policy = new AuthorizationPolicyBuilder()
                  .RequireAuthenticatedUser()
                  .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<EFDatabaseContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("CoGotujoDatabase")));

            services.AddDbContext<IdentityUserDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("IdentityDbContext")));

            services.AddSignalR();

            //Dependency Injection
            #region DatabaseContext
            services.AddScoped<IDataContext, EFDatabaseContext>();
            services.AddScoped<IdentityUserDbContext>();
            #endregion

            #region Repositories
            services.AddTransient<IDishIngredientRepository, DishIngredientRepository>();
            services.AddTransient<IDishRepository, DishRepository>();
            services.AddTransient<IDishStateRepository, DishStateRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<ISimpleIngredientRepository, SimpleIngredientRepository>();
            services.AddTransient<ISingleDishRepository, SingleDishRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            #endregion

            #region Services
            services.AddTransient<IDishIngredientService, DishIngredientService>();
            services.AddTransient<IDishService, DishService>();
            services.AddTransient<IDishStateService, DishStateService>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<ISingleDishService, SingleDishService>();
            services.AddTransient<ISimpleIngredientService, SimpleIngredientService>();
            #endregion
            

            services.AddIdentityCore<User>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<IdentityUserDbContext>()
                .AddSignInManager()
                .AddDefaultTokenProviders();

            services.AddAuthentication().AddCookie("Identity.Application");
            services.AddAuthentication().AddCookie("Identity.External");
            services.AddAuthentication().AddCookie("Identity.TwoFactorUserId");
            services.AddAuthentication("Identity.Application");

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "cogotujo.pl";
                //options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(15);
                options.LoginPath = "/Account/Login";
                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                //options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                //options.SlidingExpiration = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdministratorsOnly" ,policy=>
                {
                    policy.RequireRole("Admin");
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //env.EnvironmentName = EnvironmentName.Production;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Shared/Error");
                app.UseHsts();
            }
          

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();//do autentykacji
            app.UseCookiePolicy();
            app.UseSignalR(routes =>
            {
                routes.MapHub<WaiterCookCommunicationHub>("/waiterCookCommunicationHub");
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );
            });
        }
    }
}
