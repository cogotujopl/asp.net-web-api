#!/bin/bash
set -euo pipefail

dotnet build /app/UnitTests.csproj
dotnet test /app/UnitTests.csproj  